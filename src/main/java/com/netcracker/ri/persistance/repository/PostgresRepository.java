package com.netcracker.ri.persistance.repository;

import com.netcracker.ri.persistance.entity.TestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostgresRepository extends JpaRepository<TestEntity, Long> {
    
}
