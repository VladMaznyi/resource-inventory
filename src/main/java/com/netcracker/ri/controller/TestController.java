package com.netcracker.ri.controller;

import com.netcracker.ri.persistance.entity.TestEntity;
import com.netcracker.ri.persistance.repository.PostgresRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;

@Controller
@RequestMapping("/api/v1/entity")
public class TestController {

    private PostgresRepository postgresRepository;

    @Inject
    public TestController(PostgresRepository postgresRepository) {
        this.postgresRepository = postgresRepository;
    }

    @ResponseBody
    @GetMapping(value = "/test/{name}")
    public void test(@PathVariable String name) {
        TestEntity testEntity = new TestEntity();
        testEntity.setName(name);
        testEntity.setId(123L);
        System.out.println(postgresRepository.save(testEntity));
    }
}
