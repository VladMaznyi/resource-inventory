package com.netcracker.ri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResourceInventoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResourceInventoryApplication.class, args);
    }
}
